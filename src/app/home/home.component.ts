import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { People } from '../shared/People';
import { AuthServiceService } from '../services/auth-service.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  baseURL: string = 'https://swapi.dev/api/';
  characters: People[] | undefined;
  firstCharacter: People | undefined;
  secondCharacter: People | undefined;
  subscription: Subscription = new Subscription();

  constructor(
    private http: HttpClient,
    public authService: AuthServiceService
  ) {}

  ngOnInit(): void {
    this.subscription = this.http
      .get(this.baseURL + 'people/', { observe: 'response' })
      .subscribe((data: any) => {
        if (data.body !== null) {
          this.characters = data.body.results;
        }
      });
  }

  selectFirstCharacter(character: People): void {
    this.firstCharacter = undefined;
    if (this.validateHomeworld(character.homeworld)) {
      this.http.get(character.homeworld).subscribe((data: any) => {
        if (data !== null) {
          character.homeworld = data.name;
          this.firstCharacter = character;
        }
      });
    } else {
      character.homeworld = character.homeworld;
      this.firstCharacter = character;
    }
  }

  selectSecondCharacter(character: People): void {
    this.secondCharacter = undefined;
    if (this.validateHomeworld(character.homeworld)) {
      this.http.get(character.homeworld).subscribe((data: any) => {
        if (data !== null) {
          character.homeworld = data.name;
          this.secondCharacter = character;
        }
      });
    } else {
      character.homeworld = character.homeworld;
      this.secondCharacter = character;
    }
  }

  validateHomeworld(str: string): boolean {
    var tarea = str;
    if (tarea.indexOf('http://') == 0 || tarea.indexOf('https://') == 0) {
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

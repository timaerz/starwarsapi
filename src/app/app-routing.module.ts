import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AccessGuardService } from './services/access-guard.service';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', data: { requiresLogin: true }, canActivate: [AccessGuardService], component: HomeComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface HttpToken {
  value: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthServiceService {
  isLoggedin: boolean = false;

  constructor(private router: Router, private http: HttpClient) {}

  checkAuthStatus(): boolean {
    let token = this.getToken();
    return token != null;
  }

  getToken(): string | null {
    let token = window.localStorage.getItem('Token');
    if (token) {
      let t = JSON.parse(token) as HttpToken;
      if (!t) return null;

      return t.value;
    }

    return null;
  }

  login(email: string, password: string): void {
    this.http
      .post(
        'http://localhost:8080/login',
        {
          email: email,
          password: password,
        },
        { observe: 'response' }
      )
      .subscribe((data) => {
        if (data.status == 200) {
          this.isLoggedin = true;
          this.router.navigate(['/home']);

          this.setToken(data.body as HttpToken);
        } else {
          alert(
            'error with status=' +
              data.status +
              ' statusText=' +
              data.statusText
          );
        }
      });
  }

  setToken(value: HttpToken | null): void {
    let result = '';
    if (value != null) result = JSON.stringify(value);

    window.localStorage.setItem('Token', result);
  }

  logout(): void {
    let token = this.getToken();
    if (token == null) token = '';

    const headers = new HttpHeaders().set('Token', token as string);

    this.http
      .post('http://localhost:8080/logout', null, { headers: headers })
      .subscribe(() => {
        this.setToken(null);
        this.router.navigate(['/login', { replaceUrl: true }]);
      });
  }
}
